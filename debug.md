Violett
-------
Die Kalibrierung kann nicht beendet werden. ==> Prüfen, ob die Sensoren im richtigen Abstand und Winkel zu einem Sound-reflektiven Material sind.

Statuscodes
=======
Die unterste LED zeigt den Status des inneren, die oberste LED die des oeberen Sensors an. Diese Anzeige blinkt im Intervall von 1 Sekunde. Sollte die Anzeige statisch sein, hat sich das Gerät aufgehängt. ==> Neu starten

(Wenn beide Zustände erfüllt sind, leuchtet die Anzeige violett.)

Violett
-------
Der Sensor erhält kein Echo. (Abstand zu reflektiver Fläche zu hoch)

Cyan
-------
Der Sensor ist ausgelöst.

Dunkles Weiß
-------
Keines von beidem


Fehlercodes für die einzelnen Sensoren
=======
Der untere (erste) Teil zeigt einen Fehler bei dem inneren Sensor, der obere Teil einen Fehler bei dem äuerenen Sensor an.

Blau
-------
Der Sensor erhält kein Echo. ==> Prüfen, ob sich die Sensoren im richtigen Abstand zu einem reflektiven Material befinden und neu starten.

Cyan
-------
Der Sensor ist dauerhaft ausgelöst. ==> Prüfen ob der Sensor richtig sitzt und neu starten.