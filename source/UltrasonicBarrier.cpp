/*
  UltrasonicBarrier.cpp
  (c) Torben Eims 2020
*/

#include "Arduino.h"
#include "UltrasonicBarrier.h"

unsigned int UltrasonicBarrier::updateDistance(unsigned int max) {
  if (max == 0) return 0;

  // Sending an ultrasonic pulse
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);

  // Reading the reflection time
  distance = pulseIn(echoPin, HIGH, max * cmToMillisFactor) / cmToMillisFactor; // ==> NEVER PASS 0 as arg3!!! (Is this still true?)
  return distance;
}

UltrasonicBarrier::UltrasonicBarrier(byte pingPin, byte echoPin) {
  this-> pingPin = pingPin;
  this-> echoPin = echoPin;
  
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void UltrasonicBarrier::calibrate() {
  referenceDistance = updateDistance(500);

  // Recalibrate if sensor is triggered
  delay(100);
  update();
  if (isTriggered) calibrate();
}

void UltrasonicBarrier::update() {
  updateDistance(referenceDistance * maxFactor/100);

  // Updating triggered
  wasTriggered = isTriggered;
  // Is triggered
  if (distance <= referenceDistance * minFactor/100) {
  	isTriggered = true;
  	lastTriggeredAt = millis();
  // Was not triggered for <untriggerCooldown>
  } else if (lastTriggeredAt + untriggerCooldown < millis())
	  isTriggered = false;
  return;
}
