/*
  UltrasonicBarrier.h
  (c) Torben Eims 2020
*/

#ifndef UltrasonicBarrier_h
#define UltrasonicBarrier_h

#include "Arduino.h"

class UltrasonicBarrier {
  public:
    // settings
    unsigned int untriggerCooldown = 500;
  	byte minFactor = 80;
  	byte maxFactor = 200;

    // values
    unsigned long lastTriggeredAt = 0;
    unsigned int distance;
    bool isTriggered = false, wasTriggered = false;
    UltrasonicBarrier(byte pingPin, byte echoPin);

    // functions
    void calibrate();
    void update();
  protected:
    // constant
    const byte cmToMillisFactor = 29 * 2;

    // settings
    byte pingPin, echoPin;
    unsigned int referenceDistance;

    
    unsigned int updateDistance(unsigned int max);
};

#endif
