#include "Arduino.h"
#include "CustomNeopixel.h"
#include <Adafruit_NeoPixel.h>

void CustomNeopixel::update() {
  if (interval > 0) {
    if ((millis()-startOffset) % (interval*2) < interval)
      fill(flashColors[0]);
    else fill(flashColors[1]);
    show();
  }
  return;
}

void CustomNeopixel::flashStart(uint32_t color1, uint32_t color2, unsigned int interval) {
  flashColors[0] = color1;
  flashColors[1] = color2;
  this -> interval = interval;
  startOffset = millis() % interval;
}

void CustomNeopixel::flashStop() {interval = 0;}

void CustomNeopixel::solid(uint32_t color) {
  flashStop();
  fill(color);
  show();
}