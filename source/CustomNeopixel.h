#ifndef CustomNeopixel_h
#define CustomNeopixel_h

#include "Arduino.h"
#include <Adafruit_NeoPixel.h>

class CustomNeopixel : public Adafruit_NeoPixel {
  public:
    CustomNeopixel(byte count, uint32_t pin) : Adafruit_NeoPixel(count, pin, NEO_GRB + NEO_KHZ800) {};
    void update();
    void flashStart(uint32_t color1, uint32_t color2, unsigned int interval);
    void flashStop();
    void solid(uint32_t color);
  private:
    uint32_t flashColors[2];
    unsigned int interval;
    long startOffset;
};

#endif