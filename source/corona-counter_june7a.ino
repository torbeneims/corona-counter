#include <Adafruit_NeoPixel.h>
#include "UltrasonicBarrier.h"
#include "CustomNeopixel.h"

// Constants (time values in seconds)
const unsigned int invalidStateTimeout = 15;      // When the state should be reset, if it is invalid
const unsigned int occupiedTimeout = 5 * 60;      // When the number of people should be decreaded, if no one left
const unsigned int powersaveTimeout = 3 * 60;     // When the display should turn off, if there are no people inside
const byte allowedIn = 2;                         // How many people are allowed inside

// Sensors are defined as (trigger, echo)
UltrasonicBarrier outer(1, 3);  // Outer sensor, the sensor that is triggered first, when someone enters
UltrasonicBarrier inner(0, 4);  // Inner sensor
CustomNeopixel strip(16, 2);    // LedStrip display

// Variables
byte peopleIn = 0, prevPeopleIn = 99; // How many people are currently inside, ~ were previously inside
unsigned long lastInAt;              // millis() when the previous person left
typedef enum {
  undefinedId = 0,
  innerId = 1,
  outerId = 2,
} SensorId;
SensorId lastTriggeredSensor = undefinedId; // The sensor that was last triggered

void setup() {
  delay(3000);
  // Initialising the display
  strip.begin();
  strip.clear();
  strip.setBrightness(255 * .5);
  strip.solid(0xFF00FF);

  // Setting sensor constants
  outer.untriggerCooldown = 1000;
  inner.untriggerCooldown = 1000;
  // Calibrating the sensors
  outer.calibrate();
  inner.calibrate();
  
  //Calibration done
  strip.solid(0x0000FF);
}

void loop() {
  // Updating the display (used for flash())
  strip.update();
  // Initialising the millis at start of iteration
  unsigned long start = millis();

  // ++++++++++++++++++++++++++++++++++++
  // ++++++++++ Sensor Updates ++++++++++
  // ++++++++++++++++++++++++++++++++++++
  // Update the sensors
  outer.update();
  inner.update();
  
  // Updating values if ###OUTER### is newly triggered
  if (outer.isTriggered and not outer.wasTriggered) {
    // Someone left
    if (lastTriggeredSensor == innerId)
      peopleIn--;
    
    // Resetting direction
    if (lastTriggeredSensor != undefinedId) lastTriggeredSensor = undefinedId;
    // Someone triggered the outer sensor (likely going in)
    else lastTriggeredSensor = outerId;
  }
  
  // Updating values if ###INNER### is newly triggered
  if (inner.isTriggered and not inner.wasTriggered) {
    // Someone entered
    if (lastTriggeredSensor == outerId) {
      peopleIn++;
      lastInAt = start;
    }
    
    // Resetting direction
    if (lastTriggeredSensor != undefinedId) lastTriggeredSensor = undefinedId;
    
    // Someone triggered the inner sensor (likely going out)
    else lastTriggeredSensor = innerId;
  }

  // ++++++++++++++++++++++++++++++++++++
  // ++++++++++ Error Handling ++++++++++
  // ++++++++++++++++++++++++++++++++++++
  
  // Last trigger is more then <invalidStateTimeout> seconds ago
  if (lastTriggeredSensor != undefinedId and inner.lastTriggeredAt + 1000L*invalidStateTimeout < start and outer.lastTriggeredAt + 1000L*invalidStateTimeout < start) {
    lastTriggeredSensor = undefinedId;
  }

  // Last person entered more than <occupiedTimeout> seconds ago but there is still someone in, get 'em out
  if (lastInAt + 1000L*occupiedTimeout < start and peopleIn > 0) {
    peopleIn--;
    lastInAt = start;
  }

  // Someone would need to get in for the room to be completely empty
  if (peopleIn > 100)
    peopleIn = 0;

  // +++++++++++++++++++++++++++++
  // ++++++++++ Display ++++++++++
  // +++++++++++++++++++++++++++++
  
  if (peopleIn != prevPeopleIn) {
    // There are too many people in ==> FLASH RED 
    if (peopleIn > allowedIn)
      strip.flashStart(0, 0xFF0000, 1000);
    // There are the maximum number people in ==> RED
    else if (peopleIn == allowedIn) 
      strip.solid(0xFF0000);
    // 2/3 or more ==> YELLOW
    else if (peopleIn >= floor(2*allowedIn/3)) 
      strip.solid(0xAAAA00);
    // Less then 2/3 ==> GREEN
    else strip.solid(0x00FF00);
  }
  // Powersave, turn the lights off
  if (start - lastInAt >= 1000L*powersaveTimeout and peopleIn == 0)
    strip.solid(0x000000);

  // Setting prev vaules
  prevPeopleIn = peopleIn;
}
